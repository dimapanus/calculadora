import java.util.Scanner;

public class Calculadora {

	static Scanner scn = new Scanner(System.in);

	static int numA;
	static int numB;
	static int res;
	static boolean acumulado = false;

	public static void main(String[] args) {

		int opcio = 0;

		do {

			// Llegeix la opcio
			opcio = menu();

			// Comprova la opcio
			switch (opcio) {

			case 1:

				if (!acumulado)
					numA = obtenerNum();

				numB = obtenerNum();
				break;

			case 2:
				res = Suma(numA, numB);
				break;

			case 3:

				res = resta(numA, numB);
				break;

			case 4:

				res = multiplicar(numA, numB);
				break;

			case 5:

				res = divison(numA, numB);

				break;

			case 6:
				System.out.println(res);
				break;

			}

			System.out.println("");

		} while (opcio != 0);

		scn.close();

	}

	public static int menu() {

		System.out.println("1. Obtenir Numeros");
		System.out.println("2. Suma");
		System.out.println("3. Resta");
		System.out.println("4. Multiplicacion");
		System.out.println("5. Division");
		System.out.println("6. Mostrar Resultado");
		System.out.println("0. Salir");

		return scn.nextInt();

	}

	private static int obtenerNum() {
		int num = 0;
		System.out.println("Introduce un numero para operar ");
		num = scn.nextInt();

		return num;
	}

	public static int Suma(int num1, int num2) {

		int resultat = 0;
		if (!acumulado) {
			resultat = num1 + num2;
			acumulado = true;
		} else
			resultat = res + num2;

		return resultat;

	}

	private static int resta(int num1, int num2) {

		int resultat = 0;
		if (!acumulado) {
			resultat = num1 - num2;
			acumulado = true;
		} else
			resultat = res - num2;
		return resultat;

	}

	private static int multiplicar(int num1, int num2) {

		int resultat = 0;

		if (!acumulado) {
			resultat = num1 * num2;
			acumulado = true;
		} else
			resultat = res * num2;
		return resultat;

	}

	private static int divison(int numA2, int numB2) {

		int opcioD = 0;
		int resultat=0;
		System.out.println("Que tipo de division quieres");
		System.out.println("1.- Entera ");
		System.out.println("2.- Modul ");
		opcioD = scn.nextInt();

		switch (opcioD) {

		case 1:
			resultat = divisionEntera(numA, numB);
			break;
		case 2:
			resultat = divisionModular(numA, numB);
			break;
		}
		return resultat;
	}

	private static int divisionEntera(int num1, int num2) {
		int resultat = 0;

		if (!acumulado) {
			resultat = num1 / num2;
			acumulado = true;
		} else
			resultat = res / num2;
		return resultat;
	}

	private static int divisionModular(int num1, int num2) {
		int resultat = 0;
		if (!acumulado) {
			resultat = num1 % num2;
			acumulado = true;
		} else
			resultat = res % num2;
		return resultat;
	}
}
